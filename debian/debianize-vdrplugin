#!/bin/sh

detect_plugin_name()
{
    local pattern='.*/(?:vdr-)?(?:plugin-)?([^/]*)-(\d[\da-zA-Z+.~-]*)$'
    if pwd | grep -q -P "$pattern"; then
        PLUGIN=`pwd | perl -lpe "s|$pattern|\1|"`
        VERSION=`pwd | perl -lpe "s|$pattern|\2|"`
    else
        echo "debianize-vdrplugin expects to be executed in the directory containing"
        echo "the upstream sources. That directory should be named"
        echo "  [vdr-][plugin-]<PLUGIN-NAME>-<VERSION>,"
        echo "  e.g. vdr-plugin-coolplugin-0.0.1"
        echo "  or   vdr-coolplugin-0.0.1"
        echo "  or   plugin-coolplugin-0.0.1"
        echo "  or   coolplugin-0.0.1"
        echo 
        echo "The upsteam tarball should be named"
        echo "  [vdr-][plugin-]<PLUGIN-NAME>-<VERSION>.tar.gz,"
        echo "  e.g. vdr-coolplugin-0.0.1.tar.gz"
        exit 1
    fi
}

create_orig_tarball()
{
    local tarball

    ORIGTARBALL="../vdr-plugin-$PLUGIN"_"$VERSION.orig.tar.gz"

    if [ -e $ORIGTARBALL ] ; then
        return
    fi

    for tarball in `find ../ -regex ".*$PLUGIN-$VERSION.t.*\(gz\|bz2\)"`; do
        ln -sf `basename $tarball` $ORIGTARBALL
        return
    done

    echo "The upstream tarball was not found!"
    exit 1
}

check_dh_make()
{
    if [ ! -x /usr/bin/dh_make ] ; then
        echo "If you want to use this script, please install the package dh-make!"
        exit 1
    fi
}

replace_vdr_version()
{
    VDRVERSION=`dpkg -s vdr-dev | awk '/Version/ { print $2 }'`
    perl -pi -e "s/#VDRVERSION#/$VDRVERSION/g" debian/control
}

replace_plugin_name()
{
    perl -pi -e "s/#PLUGIN#/$PLUGIN/g" debian/install
    perl -pi -e "s/#PLUGIN#/$PLUGIN/g" debian/links.ex
}

#
# main()
#

check_dh_make
detect_plugin_name
create_orig_tarball

dh_make="/usr/bin/dh_make -d -s -t /usr/share/vdr-dev/plugin-template -p vdr-plugin-$PLUGIN"
if [ ! -e $ORIGTARBALL ] ; then
    dh_make="$dh_make -n"
fi

$dh_make

if [ "$?" -eq "0" ]; then
    replace_vdr_version
    replace_plugin_name
fi

if [ ! -d debian/source ]; then
  mkdir debian/source
  echo "3.0 (quilt)" >debian/source/format
fi
